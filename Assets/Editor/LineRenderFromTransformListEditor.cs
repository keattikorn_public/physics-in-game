﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LineRenderFromTransformList))]
public class LineRenderFromTransformListEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        LineRenderFromTransformList lrFromTransformList = (LineRenderFromTransformList)target;
        LineRenderer lineRenderer = lrFromTransformList.GetComponent<LineRenderer>();
        //lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.material.color = lrFromTransformList.LineColor;

        if (lrFromTransformList.TransformList.Length > 0)
        {

            EditorGUILayout.FloatField("Number of transform in the list", lrFromTransformList.TransformList.Length);

            Vector3[] points = new Vector3[lrFromTransformList.TransformList.Length];
            for (int i = 0; i < lrFromTransformList.TransformList.Length; i++)
            {
                points[i].x = lrFromTransformList.TransformList[i].position.x;
                points[i].y = lrFromTransformList.TransformList[i].position.y;
                points[i].z = lrFromTransformList.TransformList[i].position.z;
            }


            lineRenderer.positionCount = points.Length;
            lineRenderer.SetPositions(points);
        }

        DrawDefaultInspector();
    }
}
