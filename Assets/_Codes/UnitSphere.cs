﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KSResearch
{
    public class UnitSphere : UnitCircle
    {

        [SerializeField]
        [Tooltip("The current Beta angle in degree of the unit circle")]
        private float _betaAngleDegree = 0.0f;
        public float Beta
        {
            get
            {
                return _betaAngleDegree;
            }
        }

        public float SetBetaInDegree
        {
            set
            {
                _betaAngleDegree = value;
            }
        }
        public float SetBetaInRadian
        {
            set
            {
                _betaAngleDegree = Mathf.Rad2Deg * value;
            }
        }

        public Vector2 UnitCircleYZComponents
        {
            get
            {
                float y, z;
                y = Mathf.Cos(_betaAngleDegree);
                z = Mathf.Sin(_betaAngleDegree);
                return new Vector2(y, z);
            }
        }

        public float ZComponent
        {
            get
            {
                return Mathf.Sin(_betaAngleDegree);
            }
        }

        // Use this for initialization
        public override void Start()
        {

        }

        // Update is called once per frame
        public override void Update()
        {

        }
    }
}