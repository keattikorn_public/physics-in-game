﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class KeyDownToAddImpulseForce : MonoBehaviour
{
    [SerializeField]
    KeyCode _key;
    public KeyCode Key { get => _key; set => _key = value; }

    [SerializeField]
    Vector3 _forceDirection;

    [SerializeField]
    float _forceMagnitude = 5;

    Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(_key))
        {
            _rigidbody.AddForce(_forceDirection.normalized*_forceMagnitude,ForceMode.Impulse);
        }        
    }
}
