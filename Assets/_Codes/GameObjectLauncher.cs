﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectLauncher : MonoBehaviour
{
    [Header("Rigidbody game object to be launched")]
    [SerializeField]
    GameObject[] _gameobjects = null;

    [SerializeField]
    float _impulseMagnitude;

    [Header("Launcher interval min/max in seconds")]
    [SerializeField]
    float _launchIntervalLowerBound = 1;   
    [SerializeField]
    float _launchIntervalUpperBound = 1;   

    float _nextLaunch;

    Transform _launcherTransform;
    
    // Start is called before the first frame update
    void Start()
    {
        _launcherTransform = this.transform;

        ScheduleNextLaunch();
    }

    void ScheduleNextLaunch(){
        _nextLaunch = Random.Range(_launchIntervalLowerBound,_launchIntervalUpperBound);
        Invoke("Launch",_nextLaunch);
    }

    void Launch(){

        GameObject go = Instantiate(_gameobjects[ Random.Range(0,_gameobjects.Length) ],_launcherTransform.position,Quaternion.identity);
        Rigidbody rigidbody = go.GetComponent<Rigidbody>();

        if(rigidbody != null){
            rigidbody.AddForce(_launcherTransform.forward*_impulseMagnitude,ForceMode.Impulse);

            Destroy(go,10);
            ScheduleNextLaunch();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
