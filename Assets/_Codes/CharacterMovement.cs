﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField]
    float _pushMagnitude = 0.5f;

    [SerializeField]
    float _jumpMagnitude = 5;


    Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float verticalAxis = Input.GetAxisRaw("Vertical");
        float horizontalAxis = Input.GetAxisRaw("Horizontal");
        
        _rigidbody.AddForce(this.transform.forward*_pushMagnitude*verticalAxis,ForceMode.VelocityChange);
        _rigidbody.AddForce(this.transform.right*_pushMagnitude*horizontalAxis,ForceMode.VelocityChange);        

        if(Input.GetKeyDown(KeyCode.Space)){

            _rigidbody.AddForce(transform.up*_jumpMagnitude,ForceMode.Impulse);        

        }
    }
}
