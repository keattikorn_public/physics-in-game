﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using KSResearch.Utility;

public class RotateTowardMousePositionOnRaycasted : MonoBehaviour
{
    [SerializeField]
    bool _isShowGizmos = false;

    Vector3 _v3HitPoint= Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 v3HereToMouse = Vector3.zero;
        
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            _v3HitPoint = hit.point;
            _v3HitPoint.y = this.transform.position.y;

            v3HereToMouse = _v3HitPoint - this.transform.position; 

        }

        this.transform.LookAt(_v3HitPoint,this.transform.up);       

        if(_isShowGizmos){
            Utilities.CreateVectorGameObjectAt(this.transform.position,this.transform.forward,1,0.2f,Color.blue);
            Utilities.CreateVectorGameObjectAt(this.transform.position,this.transform.up,1,0.2f,Color.green);
            Utilities.CreateVectorGameObjectAt(this.transform.position,this.transform.right,1,0.2f,Color.red);
        }

        //Vector3.Slerp(this.transform.forward,v3HereToMouse, 1.0f);        
        //this.transform.forward = v3HereToMouse;                  
    }

    void FixedUpdate(){

    }
}
