﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LineRenderFromTransformList : MonoBehaviour
{
    [SerializeField]
    private Transform[] _transforms;
    public Transform[] TransformList {
        get { return _transforms; }
    }


    [SerializeField]
   Color _lineColor = Color.yellow;
    public Color LineColor { get => _lineColor; set => _lineColor = value; }

    LineRenderer _lineRenderer;

    Vector3[] _points;

    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        _lineRenderer.material.color = LineColor;

        _points = new Vector3[_transforms.Length];
    }

    // Update is called once per frame
    void Update()
    {
        
        for (int i = 0; i < _transforms.Length; i++)
        {
            _points[i].x = _transforms[i].position.x;
            _points[i].y = _transforms[i].position.y;
            _points[i].z = _transforms[i].position.z;
        }

        _lineRenderer.positionCount = _points.Length;
        _lineRenderer.SetPositions(_points);
    }
}
